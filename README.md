# Exporting from memory to cart file

(from inside TIC-80 editor)

Standard tic format

```
save rectsim.tic
```

External editor format

(supported languages: .lua, .rb, .js, .moon, .fnl, .nut, .wren, .wasmp)

```
save rectsim.lua
```

# Exporting as publishable HTML

```
export html rectsim
```

This will create `rectsim.zip`, which can be uploaded to itch.io and played in the browser.

Note: to continue making chnages via external editor, run the load command again after exporting to HTML (use relevant extension)

```
load rectsim.lua
```