-- title:   game title
-- author:  game developer, email, etc.
-- desc:    short description
-- site:    website link
-- license: MIT License (change this to your license of choice)
-- version: 0.1
-- script:  lua

t=0
min_rects=1
max_rects=200
rect_width_min=3
rect_width_max=15
rect_height_min=3
rect_height_max=15
rect_variance_min=0
rect_variance_max=10

sides={'left','top','right','bottom'}

circle_interval=10
triangle_interval=30
chopper_interval=30
choppers_max=30

screen_timer=time()
screen_switch_delay=200

-- extents of the rectangle draw area
rbx,rby,rbw,rbh=50,21,187,100

end_screen=3

msx,mxy,left=mouse()

rects={}
triangles={}
choppers={}
circles={}

text_screen1={}
text_screen2={}
text_screen3={}
text_all_screens={}

buttons_screen1={}
buttons_screen2={}
buttons_screen3={}
buttons_all_screens={}

button={}
function button:new(x,y,w,h,label)
    local o={
        x=x,
        y=y,
        w=w,
        h=h,
        label=label,
        c=10,
        default_c=10,
        triggered=false,
        trigger_time=-100,
        trigger_delay=75,
    }
    setmetatable(o,self)
    self.__index=self
    return o
end
function button:update()
    self:check_mouse()
end
function button:check_mouse()
    self.c=self.default_c
    if msx>=self.x and msx<=self.x+self.w and msy>=self.y and msy<=self.y+self.h then
        if self.triggered then
            self.c=6
            if time()-self.trigger_time>self.trigger_delay then self.triggered=false end
        else
            self.c=12
            if left then
                self:click()
            end
        end
    end
end
function button:click()
    self:trigger()
    self.triggered=true
    self.trigger_time=time()
end
function button:draw()
    rect(self.x-1,self.y-1,self.w+2,self.h+2,15)
    rect(self.x,self.y,self.w,self.h,self.c)
    local text=self.label
    if self.label=='incr' then text='x'..self.increment end
    local textlen=print(text,0,-100) --get text len in pixels by printing off-screen
    print(text,self.x+self.w/2-textlen/2+1,self.y+self.h/2-2)
end
function button:trigger()
    -- to be overridden
end
function make_button(x,y,w,h,label,coll)
    local btn=button:new(x,y,w,h,label)
    table.insert(coll,btn)
    return btn
end

counter={}
function counter:new(x,y,w,h,text,interactive)
    local o={
        x=x,
        y=y,
        w=w,
        h=h,
        c=12,
        cb=15,
        text=text,
        amt=0,
        offset=1,
        interactive=interactive or false,
    }
    setmetatable(o,self)
    self.__index=self
    return o
end
function counter:update()
end
function counter:draw()
    local textlen=print(self.text,0,-100)
    print(self.text,self.x+self.w/2-textlen/2+1,self.y-7)
    local printamt=self.amt
    if self.interactive then
        if printamt>99 then self.offset=3 elseif printamt>9 then self.offset=6 else self.offset=9 end
        rect(self.x-1,self.y-1,self.w+2,self.h+2,self.cb)
        rect(self.x,self.y,self.w,self.h,self.c)
    end
    local xoff,yoff=0,0
    if not self.interactive then printamt=clean_num(printamt) xoff=print(self.text,0,-100)/2+1 yoff=-9 end
    print(printamt,self.x+self.offset+xoff,self.y+2+yoff)
end
function make_counter(x,y,w,h,txt,coll,interactive)
    local txtbox=counter:new(x,y,w,h,txt,interactive)
    table.insert(coll,txtbox)
    return txtbox
end

triangle={}
function triangle:new()
    local o={
        x=math.random(52,225),
        y=math.random(30,117),
        w=math.random(6,10)/2*2,
        c=7,
    }
    o.h=o.w
    o.x2=o.x+o.w
    o.y2=o.y
    o.x3=o.x2-(o.x2-o.x)/2
    o.y3=o.y-o.w
    setmetatable(o,self)
    self.__index=self
    return o
end
function triangle:update()
    if msx>self.x and msx<self.x2 and msy>self.y3 and msy<self.y and left then
        self.delete=true
    end
    -- self.y=self.y+1
end
function triangle:draw()
    rect(self.x3-3,self.y,6,4,3)
    rectb(self.x3-3,self.y,6,4,0)
    tri(self.x-2,self.y+1,self.x2+2,self.y2+1,self.x3,self.y3-2,15)
    trib(self.x-2,self.y+1,self.x2+2,self.y2+1,self.x3,self.y3-2,self.c)
end
function make_triangle()
    local trngl=triangle:new()
    table.insert(triangles,trngl)
    text_tri_gen.amt=text_tri_gen.amt+1
    text_tri_count.amt=text_tri_gen.amt-text_tri_del.amt
    return trngl
end

circle={}
function circle:new(circ_origin)
    local o={
        x=55,
        y=math.random(30,100),
        r=math.random(2,5),
        c=math.random(0,15),
        vx=math.random(2,6),
        vy=-0.1,
        st=t,
        bounce=false,
        bounces=0,
    }
    if circ_origin=='right' then o.x=235 o.vx=o.vx*-1 end
    if circ_origin=='top' then o.x=145 o.vx=math.random(-4,4) o.y=25 end
    if o.vx<=0 and o.vx>-2 then o.vx=-1.5 end
    if o.vx>0 and o.vx<2 then o.vx=1.5 end
    setmetatable(o,self)
    self.__index=self
    return o
end
function circle:update()
    local dt=(t-self.st)/5
    self.x=self.x+self.vx
    if self.bounces<6 then
        self.y=self.y-self.vy/15
    else
        self.y=120-self.r
    end
    if self.y+self.r>120 then
        self.bounce=true
        self.vy=math.random(50,100)
        self.bounces=self.bounces+1
        text_cir_bounce.amt=text_cir_bounce.amt+1
    end
    self.vy=self.vy-0.5*2*dt
end
function circle:draw()
    circ(self.x,self.y,self.r,self.c)
    circb(self.x,self.y,self.r,0)
end
function make_circle(circ_origin)
    local cir=circle:new(circ_origin)
    table.insert(circles,cir)
    text_cir_gen.amt=text_cir_gen.amt+1
    text_cir_visible.amt=text_cir_visible.amt+1
    return cir
end

chopper={}
function chopper:new()
    local o={
        x=math.random(52,235),
        y=math.random(30,100),
        w=1,
        h=1,
        vx=math.random(1,2)/10,
        vy=math.random(1,2)/10,
        c=math.random(0,15),
    }
    setmetatable(o,self)
    self.__index=self
    return o
end
function chopper:update()
    self.x=self.x+self.vx
    self.y=self.y+self.vy
    if (self.vx>0 and self.x>235) or (self.vx<0 and self.x<52) then self.vx=self.vx*-1 end
    if (self.vy>0 and self.y>117) or (self.vy<0 and self.y<30) then self.vy=self.vy*-1 end
    for _,v in pairs(triangles) do
        if math.max(self.x,v.x) < math.min(self.x+self.w,v.x+v.w) and
           math.max(self.y,v.y) < math.min(self.y+self.h,v.y+v.h) then
            v.delete=true
        end
    end
end
function chopper:draw()
    rectb(self.x-1,self.y-1,self.w+2,self.h+2,15)
    rect(self.x,self.y,self.w,self.h,self.c)
end
function make_chopper()
    local chpr=chopper:new()
    table.insert(choppers,chpr)
    text_tri_chopper.amt=text_tri_chopper.amt+1
    return chpr
end

-- text holders
---- screen 1
text_count=make_counter(13,30,23,9,'COUNT',text_screen1,true)
text_width=make_counter(13,59,23,9,'WIDTH',text_screen1,true)
text_height=make_counter(13,88,23,9,'HEIGHT',text_screen1,true)
text_variance=make_counter(13,117,23,9,'VARIANCE',text_screen1,true)
text_runs=make_counter(15,137,0,0,'RUNS:',text_screen1)
text_generated=make_counter(90,137,0,0,'GENERATED:',text_screen1)
text_overlaps=make_counter(180,137,0,0,'OVERLAPS:',text_screen1)

---- screen 2
text_tri_count=make_counter(29,130,0,0,'TRIANGLES:',text_screen2)
text_tri_gen=make_counter(90,130,0,0,'GROWN:',text_screen2)
text_tri_chopper=make_counter(27,137,0,0,'CHOPPERS:',text_screen2)
text_tri_del=make_counter(96,137,0,0,'CHOPPED:',text_screen2)

---- screen 3
text_cir_visible=make_counter(23,130,0,0,'CIRCLES:',text_screen3)
text_cir_gen=make_counter(27,137,0,0,'LAUNCHED:',text_screen3)
text_cir_bounce=make_counter(100,137,0,0,'BOUNCES:',text_screen3)

---- all screens
text_screen=make_counter(206,15,0,0,'',text_all_screens)

---- text overrides
text_count.amt=50
text_width.amt=10
text_height.amt=10
text_variance.amt=5
text_runs.amt=1
text_screen.amt=1


-- buttons
button_run=make_button(13,5,23,11,'RUN',buttons_screen1)
button_increment=make_button(3,40,43,7,'incr',buttons_screen1)
button_count_minus=make_button(3,30,9,9,'-',buttons_screen1)
button_count_plus=make_button(37,30,9,9,'+',buttons_screen1)
button_width_minus=make_button(3,59,9,9,'-',buttons_screen1)
button_width_plus=make_button(37,59,9,9,'+',buttons_screen1)
button_height_minus=make_button(3,88,9,9,'-',buttons_screen1)
button_height_plus=make_button(37,88,9,9,'+',buttons_screen1)
button_variance_minus=make_button(3,117,9,9,'-',buttons_screen1)
button_variance_plus=make_button(37,117,9,9,'+',buttons_screen1)

button_make_triangle=make_button(6,5,38,11,'GROW',buttons_screen2)
button_make_chopper=make_button(3,34,43,11,'RECRUIT',buttons_screen2)

button_make_circle_left=make_button(49,65,11,11,'>>',buttons_screen3)
button_make_circle_top=make_button(135,18,16,11,'vv',buttons_screen3)
button_make_circle_right=make_button(227,65,11,11,'<<',buttons_screen3)

button_screen_left=make_button(216,5,9,9,'<',buttons_all_screens)
button_screen_right=make_button(226,5,9,9,'>',buttons_all_screens)

button_run.trigger_delay=300
button_increment.increment=1
button_increment.trigger_delay=300
button_make_triangle.trigger_delay=300
button_screen_left.trigger_delay=screen_switch_delay
button_screen_right.trigger_delay=screen_switch_delay
button_make_circle_left.trigger_delay=0
button_make_circle_top.trigger_delay=0
button_make_circle_right.trigger_delay=0

function button_run:trigger() generate_rects() text_runs.amt=text_runs.amt+1 end
function button_increment:trigger() if self.increment==5 then self.increment=1 else self.increment=5 end end
function button_count_minus:trigger() text_count.amt=(math.max(text_count.amt-button_increment.increment,min_rects)) end
function button_count_plus:trigger() text_count.amt=(math.min(text_count.amt+button_increment.increment,max_rects)) end
function button_width_minus:trigger() text_width.amt=(math.max(text_width.amt-1,rect_width_min)) end
function button_width_plus:trigger() text_width.amt=(math.min(text_width.amt+1,rect_width_max)) end
function button_height_minus:trigger() text_height.amt=(math.max(text_height.amt-1,rect_height_min)) end
function button_height_plus:trigger() text_height.amt=(math.min(text_height.amt+1,rect_height_max)) end
function button_variance_minus:trigger() text_variance.amt=(math.max(text_variance.amt-1,rect_variance_min)) end
function button_variance_plus:trigger() text_variance.amt=(math.min(text_variance.amt+1,rect_variance_max)) end

function button_make_triangle:trigger() make_triangle() end
function button_make_chopper:trigger() make_chopper() end

function button_make_circle_left:trigger() make_circle('left') end
function button_make_circle_top:trigger() make_circle('top') end
function button_make_circle_right:trigger() make_circle('right') end

function button_screen_left:trigger() if time()-screen_timer>screen_switch_delay then text_screen.amt=math.max(text_screen.amt-1,1) screen_timer=time() end end
function button_screen_right:trigger() if time()-screen_timer>screen_switch_delay then text_screen.amt=math.min(text_screen.amt+1,end_screen) screen_timer=time() end end


function BOOT()
    generate_rects()
end

function TIC()
    msx,msy,left=mouse()

    cls(13)
    _update()
    _draw()

    t=t+1
end

function _update()
    if text_screen.amt==1 then
        for _,v in pairs(buttons_screen1) do
            v:check_mouse()
        end
        for _,v in pairs(text_screen1) do
            v:update()
        end
    elseif text_screen.amt==2 then
        for _,v in pairs(buttons_screen2) do
            v:check_mouse()
        end
        for _,v in pairs(text_screen2) do
            v:update()
        end
    elseif text_screen.amt==3 then
        for _,v in pairs(buttons_screen3) do
            v:check_mouse()
        end
        for _,v in pairs(text_screen3) do
            v:update()
        end
    end
    if t%4==1 then
        for _,v in pairs(rects) do
            check_pos(v)
        end
    end
    for k,v in pairs(triangles) do
        v:update()
        if v.delete then triangles[k]=nil text_tri_del.amt=text_tri_del.amt+1 end
    end
    for k,v in pairs(circles) do
        v:update()
        if v.x>rbx+rbw or v.x<rbx then circles[k]=nil text_cir_visible.amt=text_cir_visible.amt-1 end
    end
    for _,v in pairs(choppers) do
        v:update()
    end
    for _,v in pairs(buttons_all_screens) do
        v:check_mouse()
    end
    generate_circle()
    generate_triangle()
    generate_choppers()
end

function _draw()
    -- draw rect drawing area
    rectb(rbx-1,rby-1,rbw+2,rbh+2,15)
    rect(rbx,rby,rbw,rbh,14)

    if text_screen.amt==1 then
        for _,v in pairs(buttons_screen1) do
            v:draw()
        end
        for _,v in pairs(text_screen1) do
            v:draw()
        end

        print("RECTANGLE GENERATOR",74,8)

        -- draw rects
        for _,v in pairs(rects) do
            rect(v.x,v.y,v.w,v.h,v.c)
            rectb(v.x,v.y,v.w,v.h,0)
        end
    elseif text_screen.amt==2 then
        print("TRIANGLE CHOPPER",84,8)
        for _,v in pairs(buttons_screen2) do
            v:draw()
        end
        for _,v in pairs(text_screen2) do
            v:draw()
        end
        for _,v in pairs(choppers) do
            v:draw()
        end
        for _,v in pairs(triangles) do
            v:draw()
        end
    elseif text_screen.amt==3 then
        print("CIRCLE LAUNCHER",86,8)
        for _,v in pairs(buttons_screen3) do
            v:draw()
        end
        for _,v in pairs(text_screen3) do
            v:draw()
        end
        for _,v in pairs(circles) do
            v:draw()
        end
    else
        print('screen '..text_screen.amt..' not defined',80,8)
    end

    for _,v in pairs(text_all_screens) do
        v:draw()
    end
    for _,v in pairs(buttons_all_screens) do
        v:draw()
    end
end

function generate_rects()
    rects = {}
    for n=1,text_count.amt do
        local a={
            id=n,
            x=math.random(52,235-text_width.amt-text_variance.amt),
            y=math.random(22,132-text_height.amt-text_variance.amt),
            w=text_width.amt+math.random(-text_variance.amt,text_variance.amt),
            h=text_height.amt+math.random(-text_variance.amt,text_variance.amt),
            c=math.random(1,16)
        }
        table.insert(rects,a)
    end
    text_generated.amt=text_generated.amt+#rects
end

function generate_circle()
    if t%circle_interval==0 then
        make_circle(sides[math.random(1,3)])
        circle_interval=math.random(20,55)
    end
end

function generate_triangle()
    if t%triangle_interval==0 then
        make_triangle()
        triangle_interval=math.random(5,45)
    end
end

function generate_choppers()
    if t%chopper_interval==0 and #choppers<choppers_max then
        make_chopper()
        chopper_interval=math.random(15,35)
    end
end

function draw_multicolored_rects()
    for i=300,1,-2 do
        rect(0,0,i,i,i%16)
    end
end

function check_pos(chk)
    -- woo nested loops
    for _,v in pairs(rects) do
        if (chk.id ~= v.id) then
            if math.max(chk.x,v.x) < math.min(chk.x+chk.w,v.x+v.w) and
               math.max(chk.y,v.y) < math.min(chk.y+chk.h,v.y+v.h) then
                -- they overlap, move them away from each other
                if chk.x < v.x then
                    chk.x=chk.x-1
                    v.x=v.x+1
                else
                    chk.x=chk.x+1
                    v.x=v.x-1
                end
                if chk.y < v.y then
                    chk.y=chk.y-1
                    v.y=v.y+1
                else
                    chk.y=chk.y+1
                    v.y=v.y-1
                end
                text_overlaps.amt=text_overlaps.amt+1
            end
        end
        -- keep them inside the limits of the rectangle draw area
        if v.x<rbx then v.x=rbx end
        if v.x+v.w>rbx+rbw then v.x=rbx+rbw-v.w end
        if v.y<rby then v.y=rby end
        if v.y+v.h>rby+rbh then v.y=rby+rbh-v.h end
    end
end

function clean_num(num)
    local result=num
    if num>1000000000000 then
        result=num/1000000000000
        return string.format("%.1f", result) .. 'T'
    end
    if num>1000000000 then
        result=num/1000000000
        return string.format("%.1f", result) .. 'B'
    end
    if num>1000000 then
        result=num/1000000
        return string.format("%.1f", result) .. 'M'
    end
    if num>1000 then
        result=num/1000
        return string.format("%.1f", result) .. 'K'
    end
    return result
end

-- <TILES>
-- 001:eccccccccc888888caaaaaaaca888888cacccccccacc0ccccacc0ccccacc0ccc
-- 002:ccccceee8888cceeaaaa0cee888a0ceeccca0ccc0cca0c0c0cca0c0c0cca0c0c
-- 003:eccccccccc888888caaaaaaaca888888cacccccccacccccccacc0ccccacc0ccc
-- 004:ccccceee8888cceeaaaa0cee888a0ceeccca0cccccca0c0c0cca0c0c0cca0c0c
-- 017:cacccccccaaaaaaacaaacaaacaaaaccccaaaaaaac8888888cc000cccecccccec
-- 018:ccca00ccaaaa0ccecaaa0ceeaaaa0ceeaaaa0cee8888ccee000cceeecccceeee
-- 019:cacccccccaaaaaaacaaacaaacaaaaccccaaaaaaac8888888cc000cccecccccec
-- 020:ccca00ccaaaa0ccecaaa0ceeaaaa0ceeaaaa0cee8888ccee000cceeecccceeee
-- </TILES>

-- <WAVES>
-- 000:00000000ffffffff00000000ffffffff
-- 001:0123456789abcdeffedcba9876543210
-- 002:0123456789abcdef0123456789abcdef
-- </WAVES>

-- <SFX>
-- 000:000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000304000000000
-- </SFX>

-- <TRACKS>
-- 000:100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
-- </TRACKS>

-- <PALETTE>
-- 000:1a1c2c5d275db13e53ef7d57ffcd75a7f07038b76425717929366f3b5dc941a6f673eff7f4f4f494b0c2566c86333c57
-- </PALETTE>

